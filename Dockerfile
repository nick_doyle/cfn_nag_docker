FROM ruby:2.5.1-alpine
WORKDIR /root/
COPY docker-entrypoint.sh .
RUN chmod +x docker-entrypoint.sh

# Download and install cfn-nag
RUN wget https://github.com/stelligent/cfn_nag/archive/master.zip
RUN unzip master.zip
RUN mv cfn_nag-master cfn_nag

WORKDIR /root/cfn_nag/
RUN gem uninstall cfn-nag -x
RUN gem build cfn-nag.gemspec
RUN gem install cfn-nag-0.0.0.gem --no-ri --no-rdoc

ENTRYPOINT [ "/root/docker-entrypoint.sh" ]
