
Docker image to run [cfn_nag](https://github.com/stelligent/cfn_nag)

CFN can either be passed in two ways:

* stdin, in which case 'cfn_nag' is run, and output is json or
* file(s) on mounted volume, output default txt, can also specify '-o json' (since this uses `cfn_nag_scan`)

# Example Usage

## Passing CFN template on stdin (simplest)

**`cat codebuild-vpc.yml | docker run --rm -i rdkls/cfn_nag`**

```
nick@minerva:~/ws/lab/devsecops/cfn_nag_docker$ cat codebuild-vpc.yml | docker run --rm -i rdkls/cfn_nag
{
  "failure_count": 1,
  "violations": [
    {
      "id": "F1000",
      "type": "FAIL",
      "message": "Missing egress rule means all traffic is allowed outbound.  Make this explicit if it is desired configuration",
      "logical_resource_ids": [
        "SecureSecurityGroup"
      ]
    }
  ]
}
```

Unfortunately output must be json, since this runs 'cfn_nag' which doesn't (optionally) use text output

## Docker mounting volume - One file

Requires the directory containing your CFN templates be mounted at /cfn (readonly please)

CMD is then specified as what you would like passed to cfn_nag_scan (normally just -i or --input-path)

**`docker run --rm -v $(pwd):/tmp rdkls/cfn_nag /tmp/codebuild-vpc.yml`**

```
nick@minerva:~/ws/lab/devsecops/cfn_nag_docker$ docker run --rm -v $(pwd):/tmp rdkls/cfn_nag /tmp/codebuild-vpc.yml
------------------------------------------------------------
/tmp/codebuild-vpc.yml
------------------------------------------------------------------------------------------------------------------------
| FAIL F1000
|
| Resources: ["SecureSecurityGroup"]
|
| Missing egress rule means all traffic is allowed outbound.  Make this explicit if it is desired configuration

Failures count: 1
Warnings count: 0
```

## Docker mounting volume - Multiple files

If you want to validate multiple CFN files in one hit.

**`docker run --rm -v $(pwd):/tmp rdkls/cfn_nag /tmp/`**

```
nick@minerva:~/ws/lab/devsecops/cfn_nag_docker$ docker run --rm -v $(pwd):/tmp rdkls/cfn_nag /tmp/
------------------------------------------------------------
/tmp/bitbucket-pipelines.DISABLED.yml
------------------------------------------------------------------------------------------------------------------------
| FAIL FATAL
|
| Illegal cfn - no Resources

Failures count: 1
Warnings count: 0
------------------------------------------------------------
/tmp/codebuild-vpc.yml
------------------------------------------------------------------------------------------------------------------------
| FAIL F1000
|
| Resources: ["SecureSecurityGroup"]
|
| Missing egress rule means all traffic is allowed outbound.  Make this explicit if it is desired configuration

Failures count: 1
Warnings count: 0
------------------------------------------------------------
/tmp/bastion-lambda.yml
------------------------------------------------------------
Failures count: 0
Warnings count: 0
```
